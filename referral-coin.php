<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.datumsquare.com
 * @since             1.0.0
 * @package           Referral_Coin
 *
 * @wordpress-plugin
 * Plugin Name:       Referral Coin
 * Plugin URI:        http://www.datumsquare.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Masood U.
 * Author URI:        http://www.datumsquare.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       referral-coin
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-referral-coin-activator.php
 */
function activate_referral_coin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-referral-coin-activator.php';
	Referral_Coin_Activator::activate();
	Referral_Coin_Activator::install_data();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-referral-coin-deactivator.php
 */
function deactivate_referral_coin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-referral-coin-deactivator.php';
	Referral_Coin_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_referral_coin' );
register_deactivation_hook( __FILE__, 'deactivate_referral_coin' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-referral-coin.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_referral_coin() {

	$plugin = new Referral_Coin();
	$plugin->run();

}
run_referral_coin();
