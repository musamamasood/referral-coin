<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.datumsquare.com
 * @since      1.0.0
 *
 * @package    Referral_Coin
 * @subpackage Referral_Coin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Referral_Coin
 * @subpackage Referral_Coin/public
 * @author     Masood U. <masood.u@allshoreresources.com>
 */
class Referral_Coin_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The database connection of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $wpdb    The wpdb of this plugin.
	 */
	private $cbcoin_referrals, $cbcoin_coins;
	public $wpdb;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		global $wpdb;
		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->wpdb = $wpdb;
		$this->cbcoin_referrals = $this->wpdb->prefix .'cbcoin_referrals';
		$this->cbcoin_coins 	= $this->wpdb->prefix .'cbcoin_coins';
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Referral_Coin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Referral_Coin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/referral-coin-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Referral_Coin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Referral_Coin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		$rc_code = array(
			'hash' 		=> ( isset($_SESSION['hash']) ) ? $_SESSION['hash'] : null,
			'nonce' 	=> wp_create_nonce( 'rc_nonce' ),
			'admin_url' => admin_url(),
			'ajaxurl' 	=> admin_url( 'admin-ajax.php' ),
		);

		wp_enqueue_script( 'validate-js', plugin_dir_url( __FILE__ ) . 'js/jquery.validate.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/referral-coin-public.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->plugin_name, 'rc', $rc_code );
	}

	/**
	* class function to validate coin code.
	*
	* @param coin code
	* @return boolean true in case the code is valid, 
	* false otherwise
	*/
	public function isValidCoin( $coin ) {
		if ( !isset($coin) ) return; 
	 	$result = $this->wpdb->get_row( "SELECT id FROM $this->cbcoin_coins WHERE code = '" . $coin ."'", ARRAY_A );
	 	if( $result['id'] > 0 ) return true;
	 	return false;
	}
    
    /**
	* class function to return coin id.
	*
	* @param coin code
	* @return coin id
	*/

	public function getCoinIdbyCode($coin) {
		$result = $this->wpdb->get_row( "SELECT id FROM $this->cbcoin_coins WHERE code = '" . $coin ."'", ARRAY_A );
	 	return $result['id'];
	}

    /**
	* class function to prepare record before inserting it into database
	*cbcoin_referrals
	* @param array containing form values
	* @return array of data ready to be inserted.
	*/

	public function prepareData($form_state,$hash, $hash_string) { 	
	 	$coin_id = $this->getCoinIdbyCode($form_state['coin_code']);
	 	$signup = isset( $form_state['signup'] ) ? $form_state['signup'] : 0;
	 	$data = array('coin_id' => $coin_id,
	 				  'recipient_name' => $form_state['recipient_name'],
	 				  'recipient_email' =>$form_state['recipient_email'],
	 				  'giver_name'=>$form_state['giver_name'],
	 				  'timestamp' => 'NOW()',
	 				  'ip'=>$hash['ip'],
	 				  'hostname'=>$hash['hostname'],
	 				  'agent'=>$hash['agent'],
	 				  'ua_type' => $hash['ua_type'],
	 				  'ua_family'=>$hash['ua_family'],
	 				  'ua_name'=>$hash['ua_name'],
	 				  'os_family'=>$hash['os_family'],
	 				  'os_family'=>$hash['os_family'],
	 				  'os_name'=>$hash['os_name'],
	 				  'device'=>$hash['device'],
	 				  'city'=>$hash['city'],
	 				  'region'=>$hash['region'],
	 				  'country'=>$hash['country'],
	 				  'postal'=>$hash['postal'],
	 				  'latitude'=>$hash['latitude'],
	 				  'longitude'=>$hash['longitude'],
	 				  'geo_timestamp'=>'',
	 				  'geo_latitude'=>'',
	 				  'geo_longitude'=>'',
	 				  'hash' => $hash_string,
	 				  'Signup' => $signup
	 		        );
	 	return $data;
	}
    
	public function insertData($preparedArray) {
	 	if(empty($preparedArray)) return false;

		$response = $this->wpdb->insert( $this->cbcoin_referrals,
			$preparedArray,
			array('%d','%s','%s','%s','%d','%d','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%f','%f','%d','%f','%f','%s','%d','%d')
		);
		if ($response === false) {
			return false;
		}
		return true;
	}

	/**
	* class function to execute queries.
	*
	* @param query string
	* @return query result
	*/
	public function queryData($query) {
		$result = $this->wpdb->get_results($query , ARRAY_N);
		return $result;
	}

    /**
	* class function to execute queries and return single record.
	*
	* @param query string
	* @return query result
	*/
	public  function getSingle( $query ) {
		$result = $this->wpdb->get_row( $query, ARRAY_A );

		if ( !$result ) {
			return false;
		}
		return $result;
	}
    
    /**
	* class function to update Geo timestamp, lat and long.
	*
	* @param data containing geo parameters
	* @return query result
	*/
	public function updateGeo() { 
		
		$lat 	= ($_POST['lat']) ? $_POST['lat'] : null;
		$lon 	= ($_POST['lon']) ? $_POST['lon'] : null;
		$hash 	= ($_POST['hash']) ? $_POST['hash'] : null;

		if ( isset( $lat ) && !empty( $lat ) && 
			 isset( $lon ) && !empty( $lon ) && 
			 isset( $hash ) && !empty( $hash ) ) {

		  		$response = $this->wpdb->update( $this->cbcoin_referrals,
					array(
						'geo_timestamp' => date("Y-m-d H:i:s"),
						'geo_latitude'  => $lat, 
						'geo_longitude' => $lon
					),
					array( 'hash' => $hash ),
					array( 
						'%d',
						'%f',
						'%f'
					), 
					array( '%s' )
				);

		  		if ($response === false) {
		  			echo false;
		  		}
		      	echo true;
		      	die();
	  	}
    }

	/**
	* class function to Send form values to HubSpot.
	*
	* @param data containing form values
	* @return boolean
	*/
	public  function sendValuesToHunspot($data) {
		if (!empty($data)) {

		    $hubspotutk      = isset( $_COOKIE['hubspotutk'] ); //grab the cookie from the visitors browser.
			$ip_addr         = $_SERVER['REMOTE_ADDR']; //IP address too.
			$hs_context      = array(
			    'hutk' => $hubspotutk,
			    'ipAddress' => $ip_addr,
			    'pageUrl' => "http://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"],
			    'pageName' => 'Canonball Referral Coins'
			);
			
			$hs_context_json = json_encode($hs_context);


			$str_post = "firstname=" . urlencode($data['recipient_name']) 
					    . "&lastname=" . urlencode($data['recipient_name']) 
					    . "&email=" . urlencode($data['recipient_email']) 
					    . "&referral_coin_code=" . urlencode($data['coin_code']) 
					    . "&coin_given_by=" . urlencode($data['giver_name']) 
					    . "&hs_context=" . urlencode($hs_context_json); //Leave this one be


			//replace the values in this URL with your portal ID and your form GUID
			$endpoint = 'https://forms.hubspot.com/uploads/form/v2/406054/150fc3a1-0aef-48ca-9d41-7f820b7364b2';

			$ch = @curl_init();
			@curl_setopt($ch, CURLOPT_POST, true);
			@curl_setopt($ch, CURLOPT_POSTFIELDS, $str_post);
			@curl_setopt($ch, CURLOPT_URL, $endpoint);
			@curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			    'Content-Type: application/x-www-form-urlencoded'
			));
			@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			@curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
			$response    = @curl_exec($ch); //Log the response from HubSpot as needed.
			$status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE); //Log the response status code
			@curl_close($ch);
			//echo $status_code . " " . $response;
					    
			if($status_code == 204) {
		    	//echo json_encode('success');
		    	return true;
			} else {
				return false;
			}
		}
	}
}
