(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	$(document).ready(function(){
		if(navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(handleGeo);
		} else {
			noLocation();
		}

		/*
		 *  Registeration page form validation
		 */
		$('#coinForm').validate({
			//debug: true,
	        onfocusout: function (element) {
	            this.element(element);
	        },
	        rules: {
	            'ReferralCoinForm[coin_code]': {
	                required: true,
	                minlength: 6,
	                maxlength: 6
	            },
	            'ReferralCoinForm[giver_name]': {
	                required: true,
	                minlength: 5
	            },
	            'ReferralCoinForm[recipient_name]': {
	                required: true,
	                minlength: 5
	            },
	            'ReferralCoinForm[recipient_email]': {
	                required: true,
	                email: true
	            }
	        },
	        messages: {
	            'ReferralCoinForm[coin_code]': 'Please enter Coin Code.',
	            'ReferralCoinForm[giver_name]': 'Please provide Giver Name.',
	            'ReferralCoinForm[recipient_name]':'Please provide Receipient Name.',
	            'ReferralCoinForm[recipient_email]':'Please enter a valid Receipient email.'
	        },
	        errorElement: 'div',
	        errorPlacement: function (error, element) {
	            element.after(error);
	        }
	    });
	});

	function handleGeo(position) {
		var hash = rc.hash;
	    console.log("Your position is " + position.coords.latitude + ", " + position.coords.longitude);
	    var data = { 'action': 'update_geo', 'hash': hash, 'lat': position.coords.latitude, 'lon': position.coords.longitude };
	    /* Posted Ajax Request */
		$.post( rc.ajaxurl, data, function(response){
			if(response == 1){
				console.log( 'Location submitted Successfully!' );
			}else{
				console.log(response);
			}
		});
	}

	function noLocation(error) {
	   alert("No location info available. Error code: " + error.code);
	}

})( jQuery );
