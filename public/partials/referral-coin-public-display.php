<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://www.datumsquare.com
 * @since      1.0.0
 *
 * @package    Referral_Coin
 * @subpackage Referral_Coin/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
