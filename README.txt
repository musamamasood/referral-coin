=== Referral Coin ===
Contributors: Masood U.
Tags: referral, coins
Requires at least: 3.0.1
Tested up to: 4.4.2
Stable tag: 4.4.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Plugin to create referral form in WordPress editor through shortcode. [referral_form]

== Description ==

This is the long description.  No limit, and you can use Markdown (as well as in the following sections).

For backwards compatibility, if this section is missing, the full length of the short description will be used, and
Markdown parsed.

A few notes about the sections above:

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `referral-coin.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Place shortcode `[referral_form]` in your templates or WordPress editor.
1. Import database scripts from db folder into WordPress database if failed while installing. 

== Frequently Asked Questions ==

= How to place referral form in WordPress =

Shortcode: [referral_form].

== Changelog ==

= 1.0 =
This version has initial plugin.