<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Referral_Coin
 * @subpackage Referral_Coin/includes
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @since      1.0.0
 * @package    Referral_Coin
 * @subpackage Referral_Coin/includes
 * @author     Masood U <masood.u@allshoreresources.com>
 */
class Referral_Coin_Shortcode {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $Aaysc_Tournament    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	protected $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $Aaysc_Tournament       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $Aaysc_Tournament, $version ) {

		$this->plugin_name = 'referral-coin';
		$this->version = '1.0.0';
	}

	/*
	 * Coach registration login form
	 *
	 * @since 	1.5.0
	 * $param 	null
	 */
	public function referral_registration_form(){
		//only show the registration form to non-logged-in member
		if ( !is_user_logged_in() ) {
			$output = $this->referral_registration_form_fields();
		}else{
			$user_id = get_current_user_id();
			$output  = $this->referral_registration_form_fields($user_id);
		}
		return $output;
	}

	/*
	 * Coach Registration form fields
	 *
	 * @since 	1.5.0
	 * $param   null
	 */
	public function referral_registration_form_fields($user_id = false){
	//if($user_id){ }
	// TODO: $user_id
		ob_start(); 
			$form = new ReferralCoinForm( 'ReferralCoinForm' );
	    return ob_get_clean();
	}

}


/**
* An abstract class implementing generic functionality for processing user's input
*
* This class encapsulates generic functions for working 
* with data coming from user forms. Descendants must only override certain 
* functions that perform context-specific tasks, like custom checking of 
* data, storing correct data, etc.
*
*/
class FormProcessor {
	public $Name;
	public $FirstTime;
	public $Values;
	public $Errors = array();
	public $success;
	public $ErrorMessageFormat = '%s';
	public $successMessageFormat = '%s';
	public $dbh;
	private $plugin_name = 'referral-coin';
	private $version = '1.0.0';

	function __construct( $Name ) {

		$plugin_public = new Referral_Coin_Public( $this->plugin_name, $this->version );
		$this->dbh =  $plugin_public;
		$this->Name = $Name;
		$this->Values = isset($_POST[$this->Name]) ? $_POST[$this->Name] : null;

		$this->FirstTime = (count($this->Values) == 0);

		if (!$this->FirstTime){
			$this->CustomCheck();
			if ( $this->IsCompleted() ){
				$this->StoreData();
			}
			$this->DisplayForm(); //echo 'sdfsd';
		} else {
			$this->DisplayForm();
		}
	}

	function IsCompleted() {
		return (!$this->FirstTime && count($this->Errors)<=0);
	}

	function CustomCheck() {}
    //abstract

	function DisplayForm() {}
    //abstract


	function StoreData() {}
    //abstract


	function Set($Name, $Value) {
		$this->$Name = $Value;
	}

	function ErrorReport($Name) {
		if (isset($this->Errors[$Name]))
			printf($this->ErrorMessageFormat, $this->Errors[$Name]);
	}

	function SuccessReport() {
		if (isset($this->success))
			printf($this->successMessageFormat, $this->success);
	}

	function GetInitialValue($Name) {
		if (isset($this->Values[$Name]))
			return $this->Values[$Name];
		else
			return false;
	}

	function InitialValue($Name) {
		echo $this->GetInitialValue($Name);
	}
} 

/**
* A small hierarchy of classes that 
* serve as object wrappers for HTML form's inputs
*/

class ReferralCoinForm extends FormProcessor {
	public $parser;
	public $ua_profile;
	public $extra_data;
	public static $hash;

	public function CustomCheck() {
		if(!$this->dbh->isValidCoin($this->Values['coin_code'])) 
			$this->Errors['coin_code'] = 'the coin code "' . $this->Values['coin_code'] . '" is invalid';

		if (strlen($this->Values['giver_name']) < 5)
			$this->Errors['giver_name'] = 'Giver Name should contain at least ' . 5 . ' symbols';

		if (strlen($this->Values['recipient_name']) < 5)
			$this->Errors['recipient_name'] = 'Recipient Name should contain at least ' . 5 . ' symbols';

		if (!$this->Values['recipient_email'])
			$this->Errors['recipient_email'] = 'Recipient email is required';
	}

	public function StoreData() {
		//global $wpdb;
		$this->_buildUASData();
		if(count($this->extra_data)) {
			self::$hash = md5(implode("|", $this->extra_data));
		}
		//$GLOBALS['hash'] = self::$hash;
		$_SESSION['hash'] = self::$hash;

		$preparedData = $this->dbh->prepareData($this->Values,$this->extra_data,self::$hash);
		//var_dump($preparedData);
		//$this->dbh->wpdb->show_errors();
		//$this->dbh->wpdb->print_error();
		$response = $this->dbh->insertData($preparedData); 
		if( $response ){
			$this->success ="User data has been saved successfully!";
			$this->dbh->sendValuesToHunspot($this->Values);
		}else{
			//$wpdb->show_errors();
			$this->Errors['coin_code'] = 'The coin code "' . $this->Values['coin_code'] . '" is invalid';
		}
	}

	private function _buildUASData() {
		$this->parser = new \UAS\Parser();
		$this->parser->SetCacheDir("uasparser_cache/");
		$this->ua_profile = $this->parser->Parse();
		$this->extra_data = $this->_buildExtraData();
		return $this;
	}

	private function _buildExtraData() {
		global $wpdb;
		$cbcoins_ip_location = $wpdb->prefix .'cbcoins_ip_location';
		$cbcoins_ip_blocks = $wpdb->prefix .'cbcoins_ip_blocks';
		$ip 	  = $_SERVER['REMOTE_ADDR'];
		$hostname = gethostbyaddr($ip);
		$ip_long  = ip2long($_SERVER['REMOTE_ADDR']);
		$ip_long  = '16777219';
		
		$iplocationQuery = $wpdb->get_row("SELECT l.country, l.region, l.city, l.latitude, l.longitude, l.postalCode FROM `$cbcoins_ip_location` as `l` JOIN `$cbcoins_ip_blocks` as `b` ON (l.locId=b.locId) WHERE '$ip_long' >= b.startIpNum AND '$ip_long' <= b.endIpNum", ARRAY_A);
		
		//$ip_location = $this->dbh->getSingle($iplocationQuery);
		$ip_location = $iplocationQuery;
		$extra_data = array(
			'ip'        =>  $ip_long,
			'hostname'  =>  $hostname,
			'agent'     =>  $_SERVER['HTTP_USER_AGENT'],
			'ua_type'   =>  $this->ua_profile['typ'],
			'ua_family' =>  $this->ua_profile['ua_family'],
			'ua_name'   =>  $this->ua_profile['ua_name'],
			'os_family' =>  $this->ua_profile['os_family'],
			'os_name'   =>  $this->ua_profile['os_name'],
			'device'    =>  $this->ua_profile['device_type'],
			'city'      =>  $ip_location['city'],
			'region'    =>  $ip_location['region'],
			'country'   =>  $ip_location['country'],
			'postal'    =>  $ip_location['postalCode'],
			'latitude'  =>  $ip_location['latitude'],
			'longitude' =>  $ip_location['longitude'],
		);
		return $extra_data;
	}

	public function DisplayForm() { ?>

		<form action="" method="POST" id="coinForm">
			<div class="success"><?php $this->SuccessReport()?></div>
			<div class="form-input">
				<label for="coin_code">Coin Code:</label>
				<?php new TextInput($this, 'coin_code', '', 'size="30" maxlength="100"')?>
			</div>
			<div class="error"><?php $this->ErrorReport('coin_code')?></div>
			<div class="form-input">
				<label for="giver_name">Giver Name:</label>
				<?php new TextInput($this, 'giver_name', '', 'size="30" maxlength="100"')?>
			</div>
			<div class="error"><?php $this->ErrorReport('giver_name')?></div>
			<div class="form-input">
				<label for="recipient_name">Recipient Name:</label>
				<?php new TextInput($this, 'recipient_name', '', 'size="30" maxlength="100"')?>
			</div>
			<div class="error"><?php $this->ErrorReport('recipient_name')?></div>
			<div class="form-input">
				<label for="recipient_email">Recipient Email:</label>
				<?php new EmailInput($this, 'recipient_email', '', 'size="30" maxlength="100"')?>
			</div>
			<div class="error"><?php $this->ErrorReport('recipient_email')?></div>
			<div class="checkbox-wrapper"  style="width:100%;overflow:hidden;">
				<div class="form-input" style="float:left;"><?php new Checkbox($this, 'signup', true, 'size="30" maxlength="100"')?></div>
				<div class="lable"  style="float:left;">Signup to our newsletter:</div>
			</div>
			<div class="form-submit">
				<input type="submit" name="submit" value="Submit" />
			</div>
		</form>
	<?php
	}
} //end of class ReferralCoin Class

/**
* An abstract class representing generic form control
*
*/
class FormControl {
	protected $Name;
	protected $form;
	protected $Value;
	protected $Attributes;
	protected $FormName;
	protected $InputName;

	function __construct($Aform, $AName, $AValue='', $AAttributes='') {
		$this->Name = $AName;
		$this->form = $Aform;
		$this->Value = 
		($this->form->FirstTime)?$AValue:($this->form->Values[$this->Name]);
		$this->Attributes = $AAttributes;
		$this->FormName = $Aform->Name; 
		$this->InputName = sprintf("%s[%s]", $this->FormName, $this->Name );

		$this->Display();
	}

	public function InputName() {
		echo $this->InputName;
	}

	public function Display() {
		echo $this->Render();
	}

	public function Render() {}
    //abstract
}

/**
* Class representing a text control
* 
*/
class TextInput extends FormControl {
	public function Render() {
		return "<input type=\"text\" name=\"". $this->InputName."\" value=\"$this->Value\" $this->Attributes id=\"$this->Name\">";
	}
}

/**
* Class representing a set of radio buttons
* 
*/
class EmailInput extends FormControl {

	function Render() {
		return "<input type=\"email\" name=\"". $this->InputName."\" value=\"$this->Value\" $this->Attributes id=\"$this->Name\">";
	}
}

/**
* Class representing a set of radio buttons
* 
*/
class Checkbox extends FormControl {

	function Render() {
		return "<input type=\"checkbox\" name=\"". $this->InputName."\" value=\"$this->Value\" $this->Attributes id=\"$this->Name\">";
	}
}