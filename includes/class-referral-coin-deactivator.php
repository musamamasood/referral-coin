<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.datumsquare.com
 * @since      1.0.0
 *
 * @package    Referral_Coin
 * @subpackage Referral_Coin/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Referral_Coin
 * @subpackage Referral_Coin/includes
 * @author     Masood U. <masood.u@allshoreresources.com>
 */
class Referral_Coin_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		global $wpdb;

		$coins 		     	= $wpdb->prefix . 'cbcoin_coins';
		$referrals  		= $wpdb->prefix . 'cbcoin_referrals';
		$ip_blocks			= $wpdb->prefix . 'cbcoins_ip_blocks';
		$ip_location		= $wpdb->prefix . 'cbcoins_ip_location';
		$wpdb->query( "DROP TABLE IF EXISTS " . $coins );
		$wpdb->query( "DROP TABLE IF EXISTS " . $referrals );
		$wpdb->query( "DROP TABLE IF EXISTS " . $ip_blocks );
		$wpdb->query( "DROP TABLE IF EXISTS " . $ip_location );
		delete_option("rc_db_version");
	}

}
