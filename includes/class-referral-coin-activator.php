<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.datumsquare.com
 * @since      1.0.0
 *
 * @package    Referral_Coin
 * @subpackage Referral_Coin/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Referral_Coin
 * @subpackage Referral_Coin/includes
 * @author     Masood U. <masood.u@allshoreresources.com>
 */
class Referral_Coin_Activator {

	/**
	 * Database Table Created.
	 *
	 * Created tables while activating plugin.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		global $wpdb;
		add_option( "rc_db_version", "1.0" );
		$charset_collate 	= $wpdb->get_charset_collate();
		$coins 		     	= $wpdb->prefix . 'cbcoin_coins';
		$referrals  		= $wpdb->prefix . 'cbcoin_referrals';
		$ip_blocks			= $wpdb->prefix . 'cbcoins_ip_blocks';
		$ip_location		= $wpdb->prefix . 'cbcoins_ip_location';

		$sqlCoins = "CREATE TABLE $coins (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			code varchar(6) NOT NULL,
			entered datetime DEFAULT '0000-00-00 00:00:00',
			time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
			UNIQUE KEY coin (code),
			PRIMARY KEY id (id)
		) $charset_collate;";
		$sqlReferrals = "CREATE TABLE $referrals (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			coin_id bigint(20) NOT NULL,
			recipient_name varchar(50) NOT NULL,
			recipient_email varchar(100) NOT NULL,
			giver_name varchar(50) NOT NULL,
			timestamp TIMESTAMP DEFAULT '0000-00-00 00:00:00' NOT NULL,
			ip bigint(100) NOT NULL,
			hostname varchar(100) NOT NULL,
			agent varchar(255) NOT NULL,
			ua_type varchar(15) NOT NULL,
			ua_family varchar(25) NOT NULL,
			ua_name varchar(25) NOT NULL,
			os_family varchar(25) NOT NULL,
			os_name varchar(25) NOT NULL,
			device varchar(25) NOT NULL,
			city varchar(25) NOT NULL,
			region varchar(25) NULL,
			country varchar(25) NULL,
			postal varchar(5) NULL,
			latitude float NOT NULL,
			longitude float NOT NULL,
			geo_timestamp TIMESTAMP NULL,
			geo_latitude float NULL,
			geo_longitude float NULL,
			hash text NOT NULL,
			Signup tinyint(1) NOT NULL,
			time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
			UNIQUE KEY coin_id (coin_id),
			PRIMARY KEY id (id)
		) $charset_collate;";
		$sqlIp_blocks = "CREATE TABLE $ip_blocks (
			-- id bigint(20) NOT NULL AUTO_INCREMENT,
		  	startIpNum bigint(10) NOT NULL,
		  	endIpNum bigint(10) NOT NULL,
		  	locId bigint(10) NOT NULL,
			time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY endIpNum (endIpNum)
		) $charset_collate;";
		$sqlIp_location = "CREATE TABLE $ip_location (
			locId bigint(10) NOT NULL AUTO_INCREMENT,
		  	country char(2) NOT NULL,
		  	region char(2) NOT NULL,
		  	city varchar(50) NOT NULL,
			postalCode char(5) NOT NULL,
			latitude float NULL,
			longitude float NULL,
			dmaCode int(11) NULL,
			areaCode int(11) NULL,
			time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY locId (locId)
		) $charset_collate;";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sqlCoins );
		dbDelta( $sqlReferrals );
		dbDelta( $sqlIp_blocks );
		dbDelta( $sqlIp_location );
	}

	/**
	 * Insert record.
	 *
	 * Insert record into database while activating plugin.
	 *
	 * @since    1.0.0
	 */
	/**
	
		TODO:
		- Import datatbase while installing plugin. 
		- automate this functionality
	
	 */
	
	public static function install_data() {
		global $wpdb;

		$coins 		     	= $wpdb->prefix . 'cbcoin_coins';
		$referrals  		= $wpdb->prefix . 'cbcoin_referrals';
		$ip_blocks			= $wpdb->prefix . 'cbcoins_ip_blocks';
		$ip_location		= $wpdb->prefix . 'cbcoins_ip_location';
		$db_name 			= DB_NAME;
		$db_user 			= DB_USER;
		$db_password 		= DB_PASSWORD;
		$db_host 			= DB_HOST;
		$path 				= get_home_path() . 'wp-content/plugins/referral-coin/db/';
		passthru("nohup mysql -u $db_user -p$db_password $db_name < $path"."cbcoin_coins.sql");
		passthru("nohup mysql -u $db_user -p$db_password $db_name < $path"."cbcoin_referrals.sql");
		passthru("nohup mysql -u $db_user -p$db_password $db_name < $path"."cbcoins_ip_blocks.sql");
		passthru("nohup mysql -u $db_user -p$db_password $db_name < $path"."cbcoins_ip_location.sql");
	}
}