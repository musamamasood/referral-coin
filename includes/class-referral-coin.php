<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.datumsquare.com
 * @since      1.0.0
 *
 * @package    Referral_Coin
 * @subpackage Referral_Coin/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Referral_Coin
 * @subpackage Referral_Coin/includes
 * @author     Masood U. <masood.u@allshoreresources.com>
 */
class Referral_Coin {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Referral_Coin_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * varible for plugins.
	 */
	protected $plugin_shortcode;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'referral-coin';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Referral_Coin_Loader. Orchestrates the hooks of the plugin.
	 * - Referral_Coin_i18n. Defines internationalization functionality.
	 * - Referral_Coin_Admin. Defines all hooks for the admin area.
	 * - Referral_Coin_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-referral-coin-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-referral-coin-i18n.php';

		/**
		 * The class responsible for user agent string parser.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-referral-uas-parser.php';

		/**
		 * The class responsible for defining all common functions that access in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-referral-coin-shortcode.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-referral-coin-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-referral-coin-public.php';

		$this->loader = new Referral_Coin_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Referral_Coin_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Referral_Coin_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Referral_Coin_Admin( $this->get_plugin_name(), $this->get_version() );
		
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Referral_Coin_Public( $this->get_plugin_name(), $this->get_version() );
		$this->plugin_shortcode = new Referral_Coin_Shortcode( $this->get_plugin_name(), $this->get_version() );
		
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		//Ajax call
		$this->loader->add_action( 'wp_ajax_update_geo', $plugin_public, 'updateGeo' );
		$this->loader->add_action( 'wp_ajax_nopriv_update_geo', $plugin_public, 'updateGeo' );
		// Add Referral form enteries and shortcode
		$this->loader->add_shortcode( 'referral_form', $this->plugin_shortcode, 'referral_registration_form' );
		$this->loader->add_action( 'init', $this, 'init_public' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Referral_Coin_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	/**
	 * Register the init function for public user.
	 *
	 * @since 1.5.0
	 * @param [type] $[name] [<description>]
	 */
	public function init_public(){
		// add new referrral from shortcode
		//$this->plugin_shortcode->aaysc_add_new_memeber();
		// if(!isset($_COOKIE['hash'])) { 
		// // 	setcookie( 'hash', '', strtotime( '+30 days' ), COOKIEPATH, COOKIE_DOMAIN );
		// 	setcookie('hash', 'val', 0, COOKIEPATH, COOKIE_DOMAIN);
		// 	if ( SITECOOKIEPATH != COOKIEPATH )
		// 		setcookie('hash', 'val', 0, SITECOOKIEPATH, COOKIE_DOMAIN);
		// }
		//fix for cookie error while login.
		// var_dump(COOKIEPATH);
		// var_dump(SITECOOKIEPATH);

		// setcookie('test', 'WP Cookie usama', 0, COOKIEPATH, COOKIE_DOMAIN);
		// if ( SITECOOKIEPATH != COOKIEPATH )
		// 	setcookie('test', 'WP Cookie usama', 0, SITECOOKIEPATH, COOKIE_DOMAIN);
		if (!session_id()) session_start();
	}
}
